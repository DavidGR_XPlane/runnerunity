using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class startController : Button
{
    // Start is called before the first frame update
    void Start()
    {
        this.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene("MainScene");
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
