using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Unity.VisualScripting;
using UnityEngine;

public class puntsController : MonoBehaviour
{
    static public int punts;
    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Points: ";
    }
    /**
     * 
     * Funci� que suma 1 punt per cada seg�n de joc
     * 
     */
    // Update is called once per frame
    void Update()
    {
        // actualitzem el contador cada frame
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Points: " + punts;
    }

    public void addPoint(int n)
    {
        punts += n;
    }
}