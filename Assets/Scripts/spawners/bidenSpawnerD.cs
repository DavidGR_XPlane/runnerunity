using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bidenSpawnerD : MonoBehaviour
{
    public GameObject joeBidenEnemy;
    public puntsController pointController;
    int velocityX;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(generarBiden());
    }

    IEnumerator generarBiden()
    {
        while (true)
        {
            GameObject biden = Instantiate(joeBidenEnemy);
            int posicio = Random.Range(-1, 4);
            biden.transform.position = new Vector3(12, /*2.6f*/ posicio, 0);
            //velocityX = (-15 * (puntsController.punts / 100));
            biden.GetComponent<Rigidbody2D>().velocity = new Vector2(-15, 0);
            biden.GetComponent<bidenController>().OnDelete += pointController.addPoint;
            yield return new WaitForSeconds(Random.Range(1, 3));
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}