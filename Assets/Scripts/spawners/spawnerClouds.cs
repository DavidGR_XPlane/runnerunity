using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Reflection;
using UnityEngine;

public class spawnerClouds : MonoBehaviour
{
    // tipus de n�vols que poden apar�ixer
    public GameObject cloud0;
    public GameObject cloud1;
    public GameObject cloud2;
    // posici� reset dels n�vols un cop han sortit del l�mit
    public GameObject cloudResPos;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(generarCloud());
    }

    IEnumerator generarCloud()
    {
        while (true)
        {
            GameObject cloud;
            //random per escollir el tipus de n�vol
            int randomCloud = Random.Range(0, 3);
            if (randomCloud == 0)
            {
                cloud = Instantiate(cloud0);
            }
            else if (randomCloud == 1)
            {
                cloud = Instantiate(cloud1);
            }
            else
            {
                cloud = Instantiate(cloud2);
            }
            //random per escollir la posici� vertical del n�vol
            int randomY = Random.Range(2, 6);
            //posicionem el n�vol
            cloud.transform.position = new Vector3(cloudResPos.transform.position.x, randomY, 0);
            //afegim velocitat
            cloud.GetComponent<Rigidbody2D>().velocity = new Vector2(-7, 0);
            yield return new WaitForSeconds(1.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
