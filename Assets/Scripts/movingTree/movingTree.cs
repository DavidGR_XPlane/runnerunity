using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingTree : MonoBehaviour
{

    public GameObject threshold;
    public GameObject resPos;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().position.x < threshold.GetComponent<Transform>().position.x)
        {
            this.GetComponent<Rigidbody2D>().transform.position = resPos.GetComponent<Transform>().position;
        }
    }
}
