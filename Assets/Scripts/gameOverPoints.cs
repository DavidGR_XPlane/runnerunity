using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOverPoints : MonoBehaviour
{

    public puntsController puntsController;

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Points: " + puntsController.punts;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
