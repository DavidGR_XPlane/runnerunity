using Newtonsoft.Json.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;

public class trumpController : MonoBehaviour
{
    public GameObject projectile;
    private int spriteJumpStrength = 20000;
    private int originalGravity = 10;
    private Boolean atFloor;

    // observer per vida 
    public delegate void lowerHp();
    public event lowerHp OnEnemyCollision;
    // vidaController
    public vidaController vidaController;

    // Start is called before the first frame update
    void Start()
    {
        //nom�s ens hem de subscriure un cop, si es col�loqu�s a update
        //cada cop ens restaria 1,2,3,4,5 i aix� successivament de vida
        this.GetComponent<trumpController>().OnEnemyCollision += vidaController.lowerHp;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) && atFloor)
        {
            jump();
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            crouch();
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            unCrouch();
        } else if (Input.GetKeyDown(KeyCode.Space))
        {
            //pending
            shootProjectile();
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controla el salt, nom�s quan el prota es a terra
        if (collision.gameObject.name == "floor")
        {
            this.atFloor = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        // Controla que el personatge ha saltat
        if (collision.gameObject.name == "floor")
        {
            this.atFloor = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            OnEnemyCollision?.Invoke();
            Destroy(collision.gameObject);
            Debug.Log("Xocat amb enemic!");
        }
    }

    /**
     * 
     * Makes Trump shoot a projectile
     *
     */
    private void shootProjectile()
    {
        GameObject projectil = Instantiate(projectile);
        projectil.GetComponent<Rigidbody2D>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
        projectil.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
    }

    /**
     * 
     * Makes Trump crouch
     * 
     */
    private void crouch()
    {
        this.GetComponent<Rigidbody2D>().transform.rotation = Quaternion.Euler(0, 0, -90);
        this.GetComponent<Rigidbody2D>().gravityScale = 400;
    }
    /**
     * 
     * Makes Trump uncrouch
     * 
     */
    private void unCrouch()
    {
        this.GetComponent<Rigidbody2D>().gravityScale = originalGravity;
        this.GetComponent<Rigidbody2D>().transform.rotation = Quaternion.identity;
    }
    /**
     * 
     * Makes Trump jump
     * 
     */
    private void jump()
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, spriteJumpStrength));
    }
}