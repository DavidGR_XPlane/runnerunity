using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class vidaController : MonoBehaviour
{
    int health = 3;
    public GameObject heart0;
    public GameObject heart1;
    public GameObject heart2;

    public void lowerHp()
    {
        health--;
        checkHp();
    }

    private void checkHp()
    {
        if (health == 2)
        {
            Destroy(heart2);
        }
        if (health == 1)
        {
            Destroy(heart1);
        }
        if (health == 0)
        {
            Destroy(heart0);
            Debug.Log("GAME OVER");
            SceneManager.LoadScene("GameOver");
        }
    }

}
